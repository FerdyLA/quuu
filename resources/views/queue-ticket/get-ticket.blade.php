<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, minimal-ui">
        <!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/color/jquery.color-2.1.2.min.js"></script> -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=5AB9pE3n0g">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=5AB9pE3n0g">
        <link rel="icon" type="image/png" sizes="194x194" href="/favicon-194x194.png?v=5AB9pE3n0g">
        <link rel="icon" type="image/png" sizes="192x192" href="/android-chrome-192x192.png?v=5AB9pE3n0g">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=5AB9pE3n0g">
        <!-- <link rel="manifest" href="/site.webmanifest?v=5AB9pE3n0g"> -->
        <link rel="mask-icon" href="/safari-pinned-tab.svg?v=5AB9pE3n0g" color="#5bbad5">
        <link rel="shortcut icon" href="/favicon.ico?v=5AB9pE3n0g">
        <meta name="msapplication-TileColor" content="#2b5797">
        <meta name="msapplication-TileImage" content="/mstile-144x144.png?v=5AB9pE3n0g">
        <meta name="theme-color" content="#26b1ff">

        <meta name="apple-mobile-web-app-title" content="Quuu: {{ $outlet->name }}">
        <link rel="manifest" href="/json/manifest/{{ $device_code }}">

        <title>Get Queue Number - {{ $outlet['name'] }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .mainBody {
                margin: auto;
                max-width: 900px;
                width: 100%;
                text-align: center;
                display: block;
            }

            .wrapper {
                width: 100%;
                /* height: 100%; */
                top: 50%;
                display: block;
                position: absolute;
                transform: translateY(-50%);
            }

            .queueType {
                width: 100%;
                height: 296px;
                border: 2px solid #000000;
                font-size: 16px;
                display: inline-block;
                text-align: center;
                vertical-align: middle;
                cursor: pointer;
            }

            .queueHeader {
                margin: 35px 0 25px 0;
                font-size: 20px;
                font-weight: bold;
            }

            .queueDetails {
                font-size: 13px;
            }

            .typeIcon {
                margin-top: 15px;
                max-width: 100%;
                max-height: 130px;
                width: auto;
                height: auto;
            }

            .logo {
                max-width: 500px;
                max-height: 200px;
                width: auto;
                height: auto;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .footer {
                font-size: 11px;
                margin-top: 30px;
                max-width: 100%;
            }

            div {
                text-align: center;
                align-items: center;
            }

            .space-top {
                margin-top: 20px;
            }


            .overlay {
                height: 0%;
                width: 100%;
                position: fixed;
                z-index: 1;
                top: 0;
                left: 0;
                background-color: rgb(0,0,0);
                background-color: rgba(0,0,0, 0.9);
                overflow-y: hidden;
                transition: 0.5s;
            }

            .overlay-content {
                position: relative;
                /* top: 25%; */
                width: 100%;
                text-align: center;
                margin-top: 30px;
            }

            .overlay a {
                padding: 8px;
                text-decoration: none;
                font-size: 36px;
                color: #818181;
                display: block;
                transition: 0.3s;
            }

            .overlay a:hover, .overlay a:focus {
                color: #f1f1f1;
            }

            .overlay .closebtn {
                position: absolute;
                top: 20px;
                right: 45px;
                font-size: 60px;
                cursor: pointer;
                z-index: 9999;
            }

            input {
                display: block;
                width: 100%;
                margin: 35px 0 25px 0;
                padding: 15px;
            }

            form {
                margin-top: 30px;
                padding: 25px;
            }

            #enterContact .queueHeader {
                color: #FFFFFF;
            }

            #enterContactBlocks {
                height:400px;
            }

            #telegram-qr {
                height: 300px;
                width: 300px;
            }

            @media screen and (max-height: 450px) {
                .overlay {overflow-y: auto;}
                .overlay a {font-size: 20px}
                .overlay .closebtn {
                    font-size: 40px;
                    top: 15px;
                    right: 35px;
                }
            }

            @media screen and (max-width: 768px) {
                input {
                    margin: 10px 0;
                    padding: 15px;
                }

                form {
                    margin: 10px 0 0 0 !important;
                    padding: 15px !important;
                }

                #enterContact .queueHeader {
                    margin: 10px 0 0 0 !important;
                }

                #enterContactBlocks {
                    height:auto !important;
                }

                #telegram-qr {
                    height: 200px !important;
                    width: 200px !important;
                }
            }
        </style>

        <script>
        // function openNav() {
        //     document.getElementById("enterContact").style.height = "100%";
        //     $.()
        // }

        $( document ).ready(function() {
            // $(".queue_type_btn").on( "click", function() {
            //     console.log( $( this ).text() );
            // });
            $('.queue_type_btn').each(function () {
                var $this = $(this);
                $this.on("click", function () {
                    // $.get("/qr/telegram/" + $this.data('queue-type'), function(data, status){
                    $.ajax({url: "/qr/telegram/" + $this.data('queue-type'), success: function(result){
                        // alert("Data: " + data + "\nStatus: " + status);
                        $("#telegram-qr").attr("src", result);

                        $('.closebtn').on("click", function () {
                            $("#enterContact").height("0%");
                        });
                    }});
                    $("#enterContact").height("100%");
                });
            });
        });
        </script>   
    </head>
    <body>
        <div id="enterContact" class="overlay">
            <span class="closebtn">&times;</span>
            <div class="overlay-content">
                <!-- <a href="#">About</a>
                <a href="#">Services</a>
                <a href="#">Clients</a>
                <a href="#">Contact</a> -->
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="queueHeader">Register below</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-12 mx-auto space-top enterContactBlocks">
                            <div class="queueHeader">Mobile:</div>
                            <form id="mobile-register">
                            <input id="mobile" placeholder="Mobile Number" type="phone">
                            <input type="submit" value="Register">
                            </form>
                        </div>
                        <div class="col-md-6 col-12 mx-auto space-top enterContactBlocks">
                            <div class="queueHeader">Telegram:</div>
                            <img id="telegram-qr" src="qr/telegram/2">
                            <div class="queueDetails space-top">Scan this QR to get notified on Telegram</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container align-middle">
            <div class="row">
                <div class="col-12">
                    @if (!empty($outlet->logo))
                        <img src="/storage/quuu-images/{{ $outlet->id }}/{{ $outlet->logo }}" class="logo space-top">
                    @else
                        <img src="/images/default-logo.png" class="logo space-top">
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="queueHeader">Select from the menu below to join the queue:</div>
                </div>
            </div>
            <div class="row">
                @php
                $count = 0;
                @endphp
                @foreach ($queue_types as $queue_type)
                <div class="col-lg-3 col-sm-6 col-12 mx-auto space-top queue_type_btn" data-queue-type="{{ $queue_type->id }}">
                        <div class="queueType" style="border-color: #{{ $queue_type->display_color }};color: #{{ $queue_type->display_color }};">
                            @if (!empty($queue_type->type_icon))
                                <img src="/storage/quuu-images/{{ $outlet->id }}/{{ $queue_type->type_icon }}" class="typeIcon">
                            @endif
                            <div class="queueHeader">
                            {{ $queue_type->display_text }}
                            </div>
                            {{ $queue_type->description }}
                        </div>
                </div>
                @if ($count%4==0 && $count!=0)
                </div>
                <div class="row">
                @endif

                @php
                $count++;
                @endphp

                @endforeach
            </div>
            <div class="row">
                <div class="footer col-12">
                Powered by <a href="http://aptsys.com.sg">Aptsys Technology Solutions</a>
                </div>
            </div>
        </div>
    </body>
</html>
