<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use QrCode;
use App\Models\Queue\QueueTypeModel;
use App\Models\Queue\QrTelegramModel;

class QRCodeController extends Controller{
    public function telegram($queue_type_id)
    {  
        $auth_code = "";
        while ($auth_code == "" || QrTelegramModel::where("auth_code",$auth_code)->first())
        {
            $auth_code = $this->generateAuthCode();
        }
        $data = array(
            "auth_code" => $auth_code,
            "queue_type_id" => $queue_type_id,
        );
        QrTelegramModel::insert($data);
        // return "data:image/png;base64, ".base64_encode(QrCode::format('png')->size(350)->generate('http://t.me/helplehbot?start=' . $auth_code));	
        // return QrCode::size(350)->generate('http://t.me/helplehbot?start=' . $auth_code);
        return "data:image/png;base64,".base64_encode(QrCode::format('png')->errorCorrection('Q')->size('350')->margin(0)->generate('http://t.me/helplehbot?start=' . $auth_code));
    }

    private function generateAuthCode()
    {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(64/strlen($x)) )),1,64);
    }
}

?>