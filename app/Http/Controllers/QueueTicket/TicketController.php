<?php

namespace App\Http\Controllers\QueueTicket;
use Illuminate\Routing\Controller;

use Illuminate\Http\Request;

use App\Models\Queue\QueueNumberModel;
use App\Models\Queue\QueueTypeModel;
use App\Models\Outlet\OutletModel;
use App\Models\Outlet\OutletCounterModel;
use App\Models\Device\DeviceModel;

class TicketController extends Controller
{
    public function getQueueNumber($device_code)
    {
        session_start();

        $device = DeviceModel::where("code",$device_code)->first();

        if (empty($_SESSION["quuuSession"]) || $device->session_id != $_SESSION["quuuSession"])
        {
            if (!$device) abort(404);

            $device->session_id = $this->generateSessionId();
            $_SESSION["quuuSession"] = $device->session_id;
            $device->save(); 
        }

        $outlet = OutletModel::where("id",$device->outlet_id)->first();
        $queue_types = QueueTypeModel::where("outlet_id",$outlet->id)->get();

        return view('queue-ticket.get-ticket')->with('outlet',$outlet)->with('queue_types',$queue_types)->with('session_id',$device->session_id)->with('device_code',$device_code);
    }

    private function generateSessionId()
    {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(64/strlen($x)) )),1,64);
    }

    public function jsonManifest($device_code)
    {
        $device = DeviceModel::where("code",$device_code)->first();
        $outlet = OutletModel::where("id",$device->outlet_id)->first();

        $manifest = json_decode('
        {
            "short_name": "Quuu",
            "name": "Quuu Online",
            "icons": [
                {
                    "src": "/android-chrome-192x192.png?v=5AB9pE3n0g",
                    "sizes": "192x192",
                    "type": "image/png"
                },
                {
                    "src": "/android-chrome-512x512.png?v=5AB9pE3n0g",
                    "sizes": "512x512",
                    "type": "image/png"
                }
            ],
            "start_url": "/queue/",
            "display": "standalone",
            "orientation": "portrait"
        }
        ',TRUE);

        $manifest['start_url'] = "/queue/" . $device_code;
        $manifest['short_name'] = "Quuu: " . $outlet->name;

        return $manifest;
    }
}