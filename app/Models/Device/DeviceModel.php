<?php

namespace App\Models\Device;

use App\Models\BaseModel;

class DeviceModel extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'outlet_device';

}