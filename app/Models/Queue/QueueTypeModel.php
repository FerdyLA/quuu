<?php

namespace App\Models\Queue;

use App\Models\BaseModel;

class QueueTypeModel extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'outlet_queue_type';

}