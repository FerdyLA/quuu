<?php

namespace App\Models\Queue;

use App\Models\BaseModel;

class QueueNumberModel extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'outlet_queue';

}