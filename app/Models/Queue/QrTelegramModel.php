<?php

namespace App\Models\Queue;

use App\Models\BaseModel;

class QrTelegramModel extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'qr_telegram_queue_auth';

}