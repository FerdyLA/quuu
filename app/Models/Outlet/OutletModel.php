<?php

namespace App\Models\Outlet;

use App\Models\BaseModel;

class OutletModel extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'outlet';

}