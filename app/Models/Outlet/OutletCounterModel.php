<?php

namespace App\Models\Outlet;

use App\Models\BaseModel;

class OutletCounterModel extends BaseModel
{
    protected $primaryKey = 'id';
    protected $table = 'outlet_counter';

}