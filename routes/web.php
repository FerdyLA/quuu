<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['namespace' => '\App\Http\Controllers\QueueTicket'], function(){
    Route::get('/queue/{device_code}','TicketController@getQueueNumber');
    Route::get('/json/manifest/{device_code}','TicketController@jsonManifest');
});

Route::get('/qr/telegram/{queue_type_id}', 'QRCodeController@telegram');